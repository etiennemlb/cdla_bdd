#ifndef CDLABDD_CORE_MACROS_H_
#define CDLABDD_CORE_MACROS_H_

#include <assert.h>

#include <config.h>

namespace cdla
{
#define CLDA_CONSTEXPR constexpr

//#if BUILD_ASSERT == 1
#ifdef BUILD_ASSERT
#define cdl_assert(exp) assert(exp)
#else
#define cdl_assert(exp) ((void)(0))
#endif

}  // namespace cdla

#endif