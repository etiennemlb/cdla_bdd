#ifndef CDLABDD_CORE_NORMALIZER_H_
#define CDLABDD_CORE_NORMALIZER_H_

#include <vector>
#include <iterator>
#include <algorithm>

#include "types.h"
#include "dependency.h"
#include "relational_scheme.h"

namespace cdla
{
class Normalizer
{
public:
    void operator()(RelationalScheme &ruf) const noexcept
    {
        SimplifyTarget(ruf);
        RemoveSuperfluousAttribute(ruf);
        RemoveNonImmediateDependency(ruf);
    }

private:
    /**
     * @brief  A, B -> C, D
     *         becomes
     *         A, B -> C
     *         A, B -> D
     * @param  &ruf: RelationalScheme
     * @retval None
     */
    void SimplifyTarget(RelationalScheme &ruf) const
    {
        std::vector<cdla::Dependency> dependencies_to_add;

        for (auto &&dependency : ruf.dependencies_)
        {
            if (dependency.second.TargetSize() == 1)
            {
                //printf("Skipping %lu\n", dependency.first);
                continue;
            }
            auto itTarget = dependency.second.TargetBegin();
            auto endTarget = dependency.second.TargetEnd();
            itTarget++;  // skip the first arrtibute

            std::vector<cdla::IdType> attributes_to_delete;
            attributes_to_delete.reserve(dependency.second.TargetSize() - 1);

            for (; itTarget != endTarget; ++itTarget)
            {
                cdla::Dependency tmp;

                auto itOrigin = dependency.second.OriginBegin();
                auto endOrigin = dependency.second.OriginEnd();

                for (; itOrigin != endOrigin; ++itOrigin)
                {
                    tmp.AddToOrigin(*itOrigin);
                }

                tmp.AddToTarget(*itTarget);
                attributes_to_delete.push_back(*itTarget);

                dependencies_to_add.push_back(std::move(tmp));
            }

            for (auto &&attribute : attributes_to_delete)
                dependency.second.RemoveFromTarget(attribute);
        }

        for (auto &&dependency : dependencies_to_add)
            ruf.AddDependency(std::move(dependency));
    }

    /**
     * @brief  A,B -> B
     *         A is superfluous
     *         
     *         A, B -> C
     *         B -> C
     *         A is superfluous
     * @param  &ruf: RelationalScheme
     * @retval None
     */
    void RemoveSuperfluousAttribute(RelationalScheme &ruf) const
    {
        std::vector<cdla::IdType> dependencies_to_delete;

        // 1st Remove dependency where the right part is a subset of the left
        // (naive dependence)
        for (auto &&dependency : ruf.dependencies_)
        {
            if (dependency.second.InOrigine(*dependency.second.TargetBegin()))
                dependencies_to_delete.push_back(dependency.first);
        }

        for (auto &&dependency : dependencies_to_delete)
        {
            ruf.RemoveDependency(dependency);
        }

        dependencies_to_delete.clear();

        // 2nd Remove unnecessary left attribute
        for (auto &&dependency : ruf.dependencies_)
        {
            // skip dependencies of the form X -> 1
            // where card(X) = 1
            if (dependency.second.OriginSize() == 1)
                continue;

            auto it = dependency.second.OriginBegin();
            auto end = dependency.second.OriginEnd();
            for (; it != end;)
            {
                cdla::IdType attribute = *it;

                // Make a copy of the current origin set minus the attribute
                // being tested for superfluousness
                SetType<cdla::IdType> origin;
                std::copy_if(dependency.second.OriginBegin(),
                             dependency.second.OriginEnd(),
                             std::inserter(origin, origin.begin()), [attribute](cdla::IdType v) {
                                 return v != attribute;
                             });

                // Generate the covering
                ruf.AttributeCovering(origin);

                // check if target is a subset of origin
                auto itTarget = dependency.second.TargetBegin();
                auto endTarget = dependency.second.TargetEnd();

                bool target_is_subset = true;
                for (; itTarget != endTarget; ++itTarget)
                {
                    // if not a subset
                    if (origin.find(*itTarget) == origin.end())
                    {
                        target_is_subset = false;
                        break;
                    }
                }

                if (target_is_subset)
                {
                    // the deletion of "*it" is a good thing as "*it" is not necessary
                    it = dependency.second.RemoveFromOrigin(it).first;
                }
                else
                {
                    // We need "*it" to get the target set
                    // go to the next
                    ++it;
                }
            }
        }
    }

    /**
     * @brief  ABC -> D
     *         ABC -> E
     *         E -> D
     *         
     *         ABC -> D is unnecessary
     * @param  &ruf: RelationalScheme
     * @retval None
     */
    void RemoveNonImmediateDependency(RelationalScheme &ruf) const
    {
        SetType<cdla::IdType> visited;

        auto it = ruf.dependencies_.begin();
        auto end = ruf.dependencies_.end();

        for (; (visited.size() < ruf.dependencies_.size()) || (it != end);)
        {
            // dirty but simple, might optimize later
            if (visited.find(it->first) != visited.end())
            {
                ++it;
                continue;
            }

            SetType<cdla::IdType> base_attribute;
            Dependency temp;

            // move the dependency localy
            temp = std::move(it->second);

            std::copy(
                temp.OriginBegin(),
                temp.OriginEnd(),
                std::inserter(base_attribute, base_attribute.begin()));

            // delete the dependency from the dependency map
            // deletion of the dependency needed for testing if the dependency is
            // obtainable by transitivity
            auto next = ruf.dependencies_.Remove(it);

            // compute attribute covering
            ruf.AttributeCovering(base_attribute);

            // check if target is a subset of origin
            auto itTarget = temp.TargetBegin();
            auto endTarget = temp.TargetEnd();

            bool target_is_subset = true;
            for (; itTarget != endTarget; ++itTarget)
            {
                // if not a subset
                if (base_attribute.find(*itTarget) == base_attribute.end())
                {
                    target_is_subset = false;
                    break;
                }
            }

            // test if target of the dependency is a sub set of the attribute coverage
            if (target_is_subset)
            {
                // if yes, the deletion of the dependency was necessary
                it = next;
            }
            else
            {
                // add it back, store the fact that we checked this dependency
                visited.emplace(ruf.AddDependency(std::move(temp)));
                // iterator dont guaranty anything when an insertion follow a
                // deletion
                it = ruf.dependencies_.begin();
                end = ruf.dependencies_.end();
            }
        }
    }
};
}  // namespace cdla

#endif