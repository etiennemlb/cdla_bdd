#ifndef CDLABDD_CORE_RELATIONAL_SCHEME_H_
#define CDLABDD_CORE_RELATIONAL_SCHEME_H_

#include <ostream>

#include "types.h"
#include "map.h"
#include "attribute.h"
#include "dependency.h"
#include "keys.h"

namespace cdla
{
class Normalizer;
class RelationalScheme
{
private:
public:
    // public types
private:
    // data
    Map<cdla::IdType, cdla::Dependency> dependencies_;
    Map<cdla::IdType, cdla::Attribute> attributes_;
    // key set
    KeySet keys_;

public:
    RelationalScheme() = default;
    ~RelationalScheme() = default;

public:
    // member function proto
    cdla::IdType AddAttribute(cdla::Attribute &&attr)
    {
        return attributes_.Add(attr);
    }

    cdla::Attribute *GetAttribute(cdla::IdType id)
    {
        return attributes_.Get(id);
    }

    const cdla::Attribute *UpdateAttribute(cdla::IdType id)
    {
        return attributes_.Get(id);
    }

    void RemoveAttribute(cdla::IdType id)
    {
        std::vector<cdla::IdType> invalid_dependencies_to_delete;

        for (auto &dep : dependencies_)
        {
            // if removing the attribute from the dep causes it to become invalid
            // we schedule its deleting
            if (dep.second.RemoveFromTarget(id).second ||
                dep.second.RemoveFromOrigin(id).second)
            {
                invalid_dependencies_to_delete.push_back(dep.first);
            }
        }

        for (auto key : invalid_dependencies_to_delete)
        {
            dependencies_.Remove(key);
        }

        attributes_.Remove(id);
    }

    cdla::IdType AddDependency(cdla::Dependency &&dep)
    {
        // We never add an invalid dependency relationship between attributes
        if (dep.OriginSize() == 0 | dep.TargetSize() == 0)
            return -1;
        return dependencies_.Add(dep);
    }

    cdla::Dependency *GetDependency(cdla::IdType id)
    {
        return dependencies_.Get(id);
    }

    const cdla::Dependency *UpdateDependency(cdla::IdType id)
    {
        return dependencies_.Get(id);
    }

    void RemoveDependency(cdla::IdType id)
    {
        dependencies_.Remove(id);
    }

    void FindKeys() noexcept
    {
        keys_.FindKeys(*this);
    }

    const SetType<cdla::IdType> &GetKey()
    {
        return keys_.keys_;
    }

    void AttributeCovering(SetType<cdla::IdType> &base_attributes) const
    {
        std::size_t attribute_count = 0;
        SetType<cdla::IdType> added_dependencies;

        do
        {
            attribute_count = base_attributes.size();
            for (auto &&dependency : dependencies_)
            {
                // printf("For dep: %lu\n", dependency.first);
                // dependency targets already added with previous attributes
                if (added_dependencies.find(dependency.first) !=
                    added_dependencies.end())
                    continue;

                bool all_origin_attribute_of_dependency_in_base_attributes = true;

                auto it = dependency.second.OriginBegin();
                auto end = dependency.second.OriginEnd();

                // for all left attribute
                for (; it != end; ++it)
                {
                    // printf("    For attr: %lu\n", *it);
                    // if an attribute in the target is not in the base_attribute set
                    // aka, we can "unlock" the dependency because we lack at least
                    // one attribute
                    if (base_attributes.find(*it) == base_attributes.end())
                    {
                        all_origin_attribute_of_dependency_in_base_attributes = false;
                        break;
                    }
                }

                // we "unlocked" the dependency
                if (all_origin_attribute_of_dependency_in_base_attributes)
                {
                    // printf("        Adding attr: %lu\n", *it);
                    // adding to base_attribute, the attributes accessible with
                    // the current attribute
                    std::copy(dependency.second.TargetBegin(),
                              dependency.second.TargetEnd(),
                              std::inserter(base_attributes, base_attributes.end()));

                    added_dependencies.emplace(dependency.first);
                }
            }
        } while (attribute_count != base_attributes.size());
    }

private:
    // private member function proto
public:
    // operator et al.
    friend std::ostream &
    operator<<(std::ostream &stream,
               const RelationalScheme &rel) noexcept
    {
        stream << (rel.dependencies_);
        stream << '\n';
        stream << (rel.attributes_);
        return stream;
    }

    friend KeySet;
    friend Normalizer;
};

}  // namespace cdla
#endif