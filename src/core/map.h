#ifndef CDLABDD_CORE_SET_H_
#define CDLABDD_CORE_SET_H_

#include <unordered_map>

#include "types.h"

namespace cdla
{
template <typename K, typename V>
class Map
{
private:
    using MapType = std::unordered_map<K, V>;

public:
    // public types
    using Iterator = typename MapType::iterator;
    using ConstIterator = typename MapType::const_iterator;

private:
    K id_counter_;
    MapType data_;

public:
    Map() : id_counter_{}, data_{} {}
    ~Map() = default;

public:
    // member function
    void Clear()
    {
        id_counter_ = 0;
        data_.clear();
    }

    K Add(V &elem)
    {
        data_[id_counter_] = std::move(elem);
        return id_counter_++;
    }

    V *Get(K id)
    {
        auto it = data_.find(id);
        if (it != data_.end())
            return &it->second;
        return nullptr;
    }

    bool Remove(K id)
    {
        return data_.erase(id) == 0 ? false : true;
    }

    Iterator Remove(ConstIterator it)
    {
        return data_.erase(it);
    }

    std::size_t size() const noexcept
    {
        return data_.size();
    }

    Iterator begin() noexcept
    {
        return data_.begin();
    }

    ConstIterator begin() const noexcept
    {
        return data_.begin();
    }

    Iterator end() noexcept
    {
        return data_.end();
    }

    ConstIterator end() const noexcept
    {
        return data_.end();
    }

private:
    // private member function proto
public:
    // operators
    // friendliness
    friend std::ostream &operator<<(std::ostream &stream,
                                    const Map<K, V> &map) noexcept
    {
        for (auto const &e : map)
            stream << (e.first) << ": " << (e.second) << '\n';

        return stream;
    }
};  // namespace cdla

}  // namespace cdla
#endif