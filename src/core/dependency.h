#ifndef CDLABDD_CORE_DEPENDENCY_H_
#define CDLABDD_CORE_DEPENDENCY_H_

#include <core/config.h>

#include <vector>
#include <ostream>

#include "types.h"

namespace cdla
{
class Dependency
{
public:
    // public types
    using ConstSetIterator = SetType<cdla::IdType>::const_iterator;

private:
    SetType<cdla::IdType> origin_;
    SetType<cdla::IdType> target_;

public:
    Dependency() = default;

    Dependency(cdla::IdType a, cdla::IdType b)
        : origin_{a},
          target_{b} {}

    Dependency(std::vector<cdla::IdType> ori, std::vector<cdla::IdType> tgt)
        : origin_(ori.begin(), ori.end()), target_(tgt.begin(), tgt.end()) {}

    Dependency(Dependency &&other) : origin_{std::move(other.origin_)}, target_{std::move(other.target_)}
    {
    }

    ~Dependency() = default;

public:
    // member function proto
    std::pair<Dependency::ConstSetIterator, bool> AddToOrigin(cdla::IdType);
    std::pair<Dependency::ConstSetIterator, bool> AddToTarget(cdla::IdType);

    std::pair<bool, bool> RemoveFromOrigin(cdla::IdType);

    std::pair<ConstSetIterator, bool>
    RemoveFromOrigin(ConstSetIterator it);

    std::pair<bool, bool> RemoveFromTarget(cdla::IdType);

    std::pair<ConstSetIterator, bool>
    RemoveFromTarget(ConstSetIterator it);

    ConstSetIterator OriginBegin() const noexcept;
    ConstSetIterator OriginEnd() const noexcept;
    bool InOrigine(cdla::IdType) const noexcept;
    std::size_t OriginSize() const noexcept
    {
        return origin_.size();
    }

    ConstSetIterator TargetBegin() const noexcept;
    ConstSetIterator TargetEnd() const noexcept;
    bool InTarget(cdla::IdType) const noexcept;
    std::size_t TargetSize() const noexcept
    {
        return target_.size();
    }

private:
    // private member function proto
    std::pair<ConstSetIterator, bool>
    RemoveFromSet(ConstSetIterator it, SetType<cdla::IdType> &);
    std::pair<bool, bool> RemoveFromSet(cdla::IdType, SetType<cdla::IdType> &);

public:
    Dependency &operator=(Dependency &&other)
    {
        origin_ = std::move(other.origin_);
        target_ = std::move(other.target_);
        return *this;
    }

    friend std::ostream &operator<<(std::ostream &stream,
                                    const Dependency &dep) noexcept
    {
        for (auto const &e : dep.origin_)
            stream << (e) << ',';

        stream << " -> ";
        for (auto const &e : dep.target_)
            stream << (e) << ',';

        return stream;
    }
};  // namespace cdla

}  // namespace cdla

#endif