#ifndef CDLABDD_CORE_KEYS_H_
#define CDLABDD_CORE_KEYS_H_

#include <core/config.h>

#include <ostream>

#include "types.h"

namespace cdla
{
class RelationalScheme;
struct KeySet
{
public:
    SetType<cdla::IdType> keys_;

public:
    KeySet() = default;
    ~KeySet() = default;

public:
    // member function proto
    void FindKeys(RelationalScheme &ruf) noexcept;

private:
    // private member function proto

public:
    // operator et al.
    friend std::ostream &
    operator<<(std::ostream &stream,
               const KeySet &keys) noexcept
    {
        for (auto const &key : keys.keys_)
            stream << (key) << '\n';
        return stream;
    }
};

}  // namespace cdla
#endif