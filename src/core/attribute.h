#ifndef CDLABDD_CORE_ATTRIBUTE_H_
#define CDLABDD_CORE_ATTRIBUTE_H_

#include <string>
#include <ostream>
namespace cdla
{
struct Attribute
{
    std::string name_;
    void *data_;

    Attribute() = default;

    Attribute(std::string name,
              void *data) noexcept : name_{name},
                                     data_{data}

    {
    }

    friend std::ostream &operator<<(std::ostream &stream,
                                    const Attribute &attr) noexcept
    {
        stream << "N: " << attr.name_ << " V: " << attr.data_;
        return stream;
    }
};

}  // namespace cdla
#endif