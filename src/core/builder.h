#ifndef CDLABDD_CORE_BUILDER_H_
#define CDLABDD_CORE_BUILDER_H_

#include <string>
#include <fstream>
#include <unordered_map>

#include "types.h"
#include "attribute.h"
#include "relational_scheme.h"

namespace cdla
{
class Builder
{
public:
    void operator()(const std::string& file, RelationalScheme& ruf) const
    {
        std::unordered_map<std::string, cdla::IdType> id_to_name;

        std::fstream in;
        in.open(file, std::fstream::in);

        std::size_t len = 0;

        in >> len;

        for (size_t i = 0; i < len; ++i)
        {
            std::size_t origin_len = 0;
            std::size_t target_len = 0;

            std::vector<cdla::IdType> origin;
            std::vector<cdla::IdType> target;
            origin.reserve(origin_len);
            target.reserve(target_len);

            in >> origin_len >> target_len;

            for (size_t j = 0; j < origin_len; ++j)
            {
                std::string name;
                in >> name;

                origin.push_back(get_id(name, id_to_name, ruf));
            }

            for (size_t j = 0; j < target_len; ++j)
            {
                std::string name;
                in >> name;

                target.push_back(get_id(name, id_to_name, ruf));
            }

            ruf.AddDependency(cdla::Dependency(origin, target));
        }
    }

    cdla::IdType get_id(std::string& name,
                        std::unordered_map<std::string,
                                           cdla::IdType>& id_to_name,
                        RelationalScheme& ruf) const
    {
        auto it = id_to_name.find(name);

        // this name already has an id
        if (it != id_to_name.end())
        {
            return it->second;
        }

        cdla::IdType id = ruf.AddAttribute(cdla::Attribute(name, nullptr));
        id_to_name[name] = id;

        return id;
    }
};
}  // namespace cdla
#endif