#ifndef CDLABDD_CORE_TYPES_H_
#define CDLABDD_CORE_TYPES_H_

#include <cstddef>

namespace cdla
{
using IdType = std::size_t;

}  // namespace cdla

#endif