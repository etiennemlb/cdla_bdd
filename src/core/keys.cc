
#include <vector>

#include "keys.h"
#include "relational_scheme.h"

// It is necessary to normalize before trying to find a key
void cdla::KeySet::FindKeys(RelationalScheme &ruf) noexcept
{
    SetType<cdla::IdType> base_key;
    SetType<cdla::IdType> missing_attributes;

    std::size_t attributes_count = ruf.attributes_.size();
    std::size_t missing_attributes_count = 0;

    // fill base_key with all the attributes
    for (auto &&attribute : ruf.attributes_)
        base_key.emplace(attribute.first);

    // for all dependencies
    for (auto &&dependency : ruf.dependencies_)
    {
        auto it = dependency.second.TargetBegin();
        auto end = dependency.second.TargetEnd();

        // remove from base_key all attributes found in the right part of a
        // dependency
        for (; it != end; ++it)
        {
            base_key.erase(*it);
            missing_attributes.emplace(*it);
        }
    }
    // we now have a base key, of "must have" attribute, those that are
    // never obtained by transitivity

    // we update the missings attributes
    SetType<cdla::IdType> temp_key = base_key;
    ruf.AttributeCovering(temp_key);

    for (auto &&not_missing_anymore_attribute : temp_key)
        missing_attributes.erase(not_missing_anymore_attribute);

    missing_attributes_count = missing_attributes.size();

    while (missing_attributes_count > 0)
    {
        cdla::IdType best_attribute;
        std::size_t best_coverage = attributes_count - missing_attributes_count;

        SetType<cdla::IdType> new_best_coverage;

        for (auto &&missing_attribute : missing_attributes)
        {
            // printf("%lu\n", missing_attribute);
            // temporary copy
            SetType<cdla::IdType> temp_key = base_key;
            temp_key.emplace(missing_attribute);

            ruf.AttributeCovering(temp_key);
            // printf("   New size %lu\n", temp_key.size());
            // printf("   Best previous size %lu\n", best_coverage);
            if (temp_key.size() > best_coverage)
            {
                best_attribute = missing_attribute;
                best_coverage = temp_key.size();
                new_best_coverage = std::move(temp_key);

                if (new_best_coverage.size() == attributes_count)
                {
                    // printf("    Got everything, leaving\n");
                    break;
                }
            }
        }

        base_key.emplace(best_attribute);
        // printf("Adding %lu to the key\n", best_attribute);
        for (auto &&not_missing_anymore_attribute : new_best_coverage)
        {
            // printf("Removing %lu from the missing attributes\n", not_missing_anymore_attribute);
            missing_attributes.erase(not_missing_anymore_attribute);
        }

        missing_attributes_count = missing_attributes.size();
    }

    keys_ = base_key;
}
