#include "dependency.h"

namespace cdla
{
// id must be a key to an attribute if you want to use this Dependency as a
// Dependency of a relational scheme
std::pair<Dependency::ConstSetIterator, bool>
Dependency::AddToOrigin(cdla::IdType id)
{
    return origin_.emplace(id);
}

// id must be a key to an attribute if you want to use this Dependency as a
// Dependency of a relational scheme
std::pair<Dependency::ConstSetIterator, bool>
Dependency::AddToTarget(cdla::IdType id)
{
    return target_.emplace(id);
}

std::pair<bool, bool>
Dependency::RemoveFromOrigin(cdla::IdType id)
{
    return RemoveFromSet(id, origin_);
}

std::pair<Dependency::ConstSetIterator, bool>
Dependency::RemoveFromOrigin(Dependency::ConstSetIterator it)
{
    return RemoveFromSet(it, origin_);
}

std::pair<bool, bool>
Dependency::RemoveFromTarget(cdla::IdType id)
{
    return RemoveFromSet(id, target_);
}

std::pair<Dependency::ConstSetIterator, bool>
Dependency::RemoveFromTarget(Dependency::ConstSetIterator it)
{
    return RemoveFromSet(it, target_);
}

Dependency::ConstSetIterator
Dependency::OriginBegin() const noexcept
{
    return origin_.cbegin();
}

Dependency::ConstSetIterator
Dependency::OriginEnd() const noexcept
{
    return origin_.cend();
}

bool Dependency::InOrigine(cdla::IdType id) const noexcept
{
    return origin_.end() != origin_.find(id);
}

Dependency::ConstSetIterator Dependency::TargetBegin() const noexcept
{
    return target_.cbegin();
}

Dependency::ConstSetIterator Dependency::TargetEnd() const noexcept
{
    return target_.cend();
}

bool Dependency::InTarget(cdla::IdType id) const noexcept
{
    target_.end() != target_.find(id);
}

std::pair<Dependency::ConstSetIterator, bool>
Dependency::RemoveFromSet(Dependency::ConstSetIterator it, SetType<cdla::IdType>& set)
{
    bool found = (it != set.end());

    std::size_t size = set.size();
    bool valid_size = size > 1;

    if (found && valid_size)
    {
        it = set.erase(it);
    }

    return std::make_pair(it,
                          (found && !valid_size) || (size == 0));
}

// to prevent creating invalid dependencies, it's impossible to have an
// empty set
std::pair<bool, bool>
Dependency::RemoveFromSet(cdla::IdType id, SetType<cdla::IdType>& set)
{
    Dependency::ConstSetIterator it = set.find(id);

    auto res = RemoveFromSet(it, set);

    return std::make_pair(res.first != it, res.second);
}
}  // namespace cdla
