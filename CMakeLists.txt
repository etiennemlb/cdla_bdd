cmake_minimum_required(VERSION 3.1)
project(CDLA_BDD)

# Config #
option(BUILD_DEBUG "Enable Debug" ON)
option(BUILD_ASSERT "Enable Assertion" ON)
option(USE_TREE_SETS "Enable tree sets instead of unordered set for attributes store" ON)

option(USE_COMPILE_OPTIONS "Use compile options from CDLA_BDDD." ON)

# Write config.h
message("Generating config file: ${CMAKE_BINARY_DIR}/src/core/config.h")
configure_file("${CMAKE_SOURCE_DIR}/src/core/config.h.in" "${CMAKE_BINARY_DIR}/src/core/config.h")

# IF(USE_TREE_SETS)
#     ADD_DEFINITIONS(-DUSE_TREE_SETS)
# ENDIF(USE_TREE_SETS)

# IF(BUILD_DEBUG)
#     ADD_DEFINITIONS(-DBUILD_DEBUG)
# ENDIF(BUILD_DEBUG)

# IF(BUILD_ASSERT)
#     ADD_DEFINITIONS(-DBUILD_ASSERT)
# ENDIF(BUILD_ASSERT)

##########


# Define executable target
include_directories("${CMAKE_BINARY_DIR}/src/")

set(srcPath "${CMAKE_SOURCE_DIR}/src/core")
message("Sources in: ${srcPath}")

file(GLOB libs
    "${srcPath}/*.h"
    "${srcPath}/*.cc"
)
set(sources "${CMAKE_SOURCE_DIR}/tests/main.cc")

#########
add_executable(CDLA_BDD ${sources} ${libs})

target_compile_features(CDLA_BDD INTERFACE cxx_std_11)

if(USE_COMPILE_OPTIONS)
    target_compile_options(
        CDLA_BDD
        INTERFACE $<$<AND:$<CONFIG:Debug>,$<NOT:$<CXX_COMPILER_ID:MSVC>>>:-O0 -g>
        INTERFACE $<$<AND:$<CONFIG:Release>,$<CXX_COMPILER_ID:Clang>>:-O3>
        INTERFACE $<$<AND:$<CONFIG:Release>,$<CXX_COMPILER_ID:GNU>>:-O3>
    )
endif()

