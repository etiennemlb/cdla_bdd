
#include <iostream>
#include <vector>

#include "../src/cdla_bdd.h"

#include <unordered_set>

int main(int argc, char *argv[])
{
    cdla::RelationalScheme r;

    std::vector<cdla::IdType> id_attr;
    std::vector<cdla::IdType> id_deps;

    for (int i = 0; i < 10; ++i)
    {
        id_attr.push_back(r.AddAttribute(cdla::Attribute(std::to_string(i), nullptr)));
    }

    cdla::Dependency dep0{{1, 2, 3}, {5}};
    cdla::Dependency dep1{{1, 2, 3}, {4}};
    cdla::Dependency dep2{{4}, {5}};

    id_deps.push_back(r.AddDependency(std::move(dep0)));
    id_deps.push_back(r.AddDependency(std::move(dep1)));
    id_deps.push_back(r.AddDependency(std::move(dep2)));

    std::cout << r << std::endl;
    r.RemoveAttribute(0);
    std::cout << r << std::endl;

    std::cout << "Keys: ";
    r.FindKeys();
    auto key = r.GetKey();
    for (auto &&a : key)
    {
        std::cout << a << ',';
    }
    std::cout << std::endl
              << std::endl;

    std::cout << "Normalizing...\n";
    cdla::Normalizer norm;
    norm(r);

    std::cout << r << std::endl;

    cdla::RelationalScheme r2;
    cdla::Builder b;
    b("/mnt/c/Users/Etienne/Desktop/cdla_bdd/tests/test_file.txt", r2);

    std::cout << r2 << std::endl;
    return 0;
}